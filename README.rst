**DemoTool** is a test project to Demo creation of a python pip installable package :

* Contains Separate requirements for test and dev installion config.
* Have versioning logic.
* have internal demo app package that can be imported from env after installed with pip.
* Properly coded and available in git for jenkins configs in future.

---------------------------------
Steps to Install the application
---------------------------------
- Clone the application and make sure you have python 3.5 installed on your system.
- Make sure you have setup tools and wheel already installed.
- Type **python setup.py bdist_wheel** in the setup.py directory and bunch of files will get created along with an wheel file in the dist dir.
- Activate virtualenv and try installing the package by typing **pip install dist/wheel-file-name-with-versions-and-specifications.whl**

-------------------------
Demo
-------------------------
open a python console and try out the following.

>>> from demotool import demo
>>> demo.print_hello()
'Hello World!'
