import unittest
# import mock
import os

from demotool import demo


class DemoToolTests(unittest.TestCase):

    def setUp(self):
        pass

    def test_hello_world(self):
        self.assertEqual(demo.print_hello(), 'Hello World!')
